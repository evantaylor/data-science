# Basic wrapper for tqdm.write() and print() functions.
# Adds ISO 8601 timestamp and Hostname to beginning of console lines.
# This is probably not the best way of doing this, but it works and is simple to understand.


def xprint(*args, **kwargs):
    '''Print Wrapper to add timestamp and hostname to print or tqdm.write() function.'''
    import time
    import os
    timestamp = str("[" + time.strftime("%Y-%m-%d %H:%M:%S") + "]")
    hostname = str("[" + str(os.uname()[1]) + "]: ")
    prefix = timestamp + hostname
    try: 
        tqdm.write(prefix, *args, **kwargs)
    except:
        print(prefix, *args, **kwargs)