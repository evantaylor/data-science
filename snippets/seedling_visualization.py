def delete_post_harvest_times(df):
    harvest1_nodes = ['N15', 'N34', 'N37', 'N31', 'N45', 'N08', 'N11', 'N14', 'N27']
    harvest1_date = "2021-03-08 12:00:00"
    df.loc[df.index >= harvest1_date, harvest1_nodes] = np.nan

    harvest2_nodes = ['N16', 'N01', 'N02', 'N30', 'N22', 'N38', 'N40', 'N46', 'N19', 'N28', 'N39', 'N42']
    harvest2_date = "2021-03-15 13:00:00"
    df.loc[df.index >= harvest2_date, harvest2_nodes] = np.nan

    harvest3_nodes = ['N10', 'N12', 'N17', 'N18', 'N23', 'N25', 'N29', 'N32', 'N36', 'N03', 'N43', 'N44']
    harvest3_date = "2021-03-23 08:00:00"
    df.loc[df.index >= harvest3_date, harvest3_nodes] = np.nan

    harvest4_nodes = ['N13', 'N20', 'N21', 'N24', 'N26', 'N33', 'N35', 'N41', 'N48', 'N04', 'N06', 'N07', 'N09']
    harvest4_date = "2021-03-30 09:00:00"
    df.loc[df.index >= harvest4_date, harvest4_nodes] = np.nan
    return df

df_plot = df_diff_all[fertilizer_algae].copy()
df_plot = df_plot.resample('5Min').mean().interpolate('linear') # Stupid thing to organize data for silly 
df_plot = delete_post_harvest_times(df_plot)

columns_plot = df_plot.columns

basedate = pd.Timestamp("2021-02-21 00:00:00-07:00") # Day 0, must be timestamp within dataset

df_plot['T'] = (df_plot.index - basedate).days
mylist = (df_plot['T'].values) - 1
frequency = 288 * 1 # Measurements per DAY, so 5 minute data = (60/5) * 24 then multiple THAT number by the number of days you want between ticks

fig, ax = plt.subplots(figsize=(20,12))

plt.plot(df_plot.index.values, df_plot[columns_plot].values)
plt.title('Fertilizer and Algae Treated', y= 1.02, fontsize= 30)
plt.legend(fertilizer_algae, loc='lower right', ncol=4, fontsize=12)
plt.xlabel('Day', fontsize=30)
plt.ylabel ("Microbial Response (mV)", fontsize = 30)
plt.yticks(fontsize = 24)
plt.xticks(df_plot.index[::frequency], mylist[::frequency], fontsize = 24)

plt.show()

